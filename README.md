Do you ever dream about about getting kicked by a AI at Rock Paper Scissor while using computer vision ?<br>
Neither do I

To run it locally, you must install Yarn and run the following command to get all the dependencies.

```bash
yarn prep
```

Then, you can run

```bash
yarn start
```

Once openned, train the app to recognize each handsigns using your camera (~15 exemples are good)<br>
The first three computer moves are random, then magic happend

You can then browse to `localhost:9966` to view the application.
