import MLP from './MLP';

const plays = {
    1: 1,
    2: 0,
    3: 2,
};

const score = {
    'player': 0,
    'computer': 0,
    'tie': 0,
};

const x = [];
const y = [];

const nn = new MLP(3, 3, 3, 0.1, 300);

let i = 0,
    tmpMove = [],
    lastWinner,
    lastMove,
    scoreResults;

function play(player) {
    const move = Array(3).fill(0);
    move[player] = 1;
    tmpMove.push(move);
    if (tmpMove.length == 2) {
        x.push(tmpMove.shift());
        y.push(tmpMove[0]);
    }
    let computer;
    if (y.length < 3) {
        computer = Math.floor(Math.random() * 3);
    } else {
        let prediction = nn.predict(lastMove).data;
        computer = (prediction.indexOf(Math.max(...prediction)) + 1) % 3;
        if (lastWinner !== 'computer') {
            nn.shuffle(x, y);
            nn.fit(x, y);
        }
    }
    const win = plays[player + computer];
    lastWinner = player === computer || win === undefined ? 'tie' : win === player ? 'player' : 'computer';
    score[lastWinner]++;
    updateScore();
    lastMove = move;

    return computer;
};

function updateScore() {
    for (let player of Object.keys(score)) {
        scoreResults[player].innerHTML = score[player];
    }
};

function init() {
    scoreResults = {};
    for (let k of Object.keys(score)) {
        scoreResults[k] = document.getElementById(k);
    }
    updateScore();
};

function reset() {
    for (let k of Object.keys(score)) {
        score[k] = 0;
    }
    updateScore();
};

export {
    init,
    play,
    reset,
};
